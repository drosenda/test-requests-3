package handler

import (
	"lhcb-ntupling-service-backend/util"
	"net/http"
)

// PingHandler handles the ping request
func PingHandler(w http.ResponseWriter, r *http.Request) {
	util.SetResponse("OK", w, http.StatusOK)
}
